<?php

    class Authors 
    {
        private $_author_id;
        private $_name;
        private $_sexe;
        private $_birth_date;
        private $_biography;
        private $_img;

        public function __construct(array $donnees)
        {
            $this->hydrate($donnees);
        }

        public function hydrate(array $donnees)
        {
            foreach($donnees as $key => $value)
            {
                $method = 'set'.ucfirst($key);

                if(method_exists($this, $method))
                {
                    $this->$method($value);
                }
            }
        }

        /**
         *  Getters
         */

        public function author_id()
        {
            return $this->_author_id;
        }
        public function name()
        {
            return $this->_name;
        }
        public function sexe()
        {
            return $this->_sexe;
        }
        public function birth_date()
        {
            return date("d-m-Y", strtotime($this->_birth_date));
        }
        public function biography()
        {
            return $this->_biography;
        }
        public function img()
        {
            return $this->_img;
        }


        /**
         *  Setters
         */

        public function setAuthor_id($author_id)
        {
            $author_id = (int) $author_id;
            if($author_id > 0)
            {
                $this->_author_id = $author_id;
            }
        }

        public function setName($name)
        {
            if(is_string($name))
            {
                $this->_name = $name;
            }
        }

        public function setSexe($sexe)
        {
            if(is_string($sexe))
            {
                $this->_sexe = $sexe;
            }
        }

        public function setBirth_date($birth_date)
        {
            $this->_birth_date = $birth_date;
        }

        public function setBiography($biography)
        {
            $this->_biography = $biography;
        }

        public function setImg($img)
        {
            $this->_img = $img;
        }
    }


?>