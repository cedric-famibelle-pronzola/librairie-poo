<?php

    class Books implements JsonSerializable
    {
        private $_book_id;
        private $_title;
        private $_description;
        private $_release_date;
        private $_edition;
        private $_category;
        private $_format;
        private $_price;
        private $_img;

        public function __construct(array $donnees)
        {
            $this->hydrate($donnees);
        }

        public function hydrate(array $donnees)
        {
            foreach($donnees as $key => $value)
            {
                $method = 'set'.ucfirst($key);

                if(method_exists($this, $method))
                {
                    $this->$method($value);
                }
            }
        }

        public function JsonSerialize()
        {
            return 
            [
                "Id du Livre" => $this->_book_id,
                "Titre" => $this->_title,
                "Description" => $this->_description,
                "Date de Sortie" => date('d-m-Y', strtotime($this->_release_date)),
                "Edition" => $this->_edition,
                "Catégorie" => $this->_category,
                "Format" => $this->_format,
                "Prix en euros" => $this->_price
            ]; 
        }

        /**
         *  Getters
         */

        public function book_id()
        {
            return $this->_book_id;
        }
        public function title()
        {
            return $this->_title;
        }
        public function description()
        {
            return $this->_description;
        }
        public function release_date()
        {
            return date("d-m-Y", strtotime($this->_release_date));
        }
        public function edition()
        {
            return $this->_edition;
        }
        public function category()
        {
            return $this->_category;
        }
        public function format()
        {
            return $this->_format;
        }
        public function price()
        {
            return $this->_price;
        }
        public function img()
        {
            return $this->_img;
        }

        /**
         *  Setters
         */
        public function setBook_id($book_id)
        {
            $book_id = (int) $book_id;
            if($book_id > 0)
            {
                $this->_book_id = $book_id;
            }
        }

        public function setTitle($title)
        {
            if(is_string($title))
            {
                $this->_title = $title;
            }
        }

        public function setDescription($description)
        {
            if(is_string($description))
            {
                $this->_description = $description;
            }
        }

        public function setRelease_date($release_date)
        {
            $this->_release_date = $release_date;
        }

        public function setEdition($edition)
        {
            $this->_edition = $edition;
        }
        public function setCategory($category)
        {
            $this->_category = $category;
        }
        public function setFormat($format)
        {
            $this->_format = $format;
        }
        public function setPrice($price)
        {
            $price = (float) $price;

            $this->_price = $price;
        }
        public function setImg($img)
        {
            $this->_img = $img;
        }
    }


?>