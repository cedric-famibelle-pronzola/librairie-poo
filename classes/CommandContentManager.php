<?php

    class CommandContentManager
    {
        private $_db;

        public function __construct($db)
        {
            $this->setDb($db);
        }

        public function setDB(PDO $db)
        {
            $this->_db = $db;
        }

        /**
         * Récupère le contenu des commande en fonction du numéro de commande
         *
         * @param integer $num_command
         * @return array Retourne un tableau d'objets
         */
        public function getNumCommand($num_command)
        {
            $num_command = (int) $num_command;

            $q = $this->_db->prepare('SELECT * FROM command_content
            WHERE num_command = '.$num_command);

            $q->execute();

            foreach ($q as $key => $value) 
            {
                $command[] = array(
                    'num_command' => (int) $value['num_command'],
                    'book_id' => (int) $value['book_id'],
                    'quantity' => (int) $value['quantity']
                );
            }

            return $command;

        }

        /**
         * Ajoute le contenue d'une commande dans la base de données
         *
         * @param CommandContent $command_content Attend un objet CommandContent en argument
         * @return void
         */
        public function add(CommandContent $command_content)
        {
            $q = $this->_db->prepare('INSERT INTO command_content(num_command, book_id, quantity)
            VALUES(:num_command, :book_id, :quantity)');

            $q->bindValue(':num_command', $command_content->num_command());
            $q->bindValue(':book_id', $command_content->book_id());
            $q->bindValue(':quantity', $command_content->quantity());

            $q->execute();

        }

        


    }


?>