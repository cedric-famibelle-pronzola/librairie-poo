<?php

    class CommandsManager
    {
        private $_db;

        public function __construct($db)
        {
            $this->setDb($db);
        }

        public function setDB(PDO $db)
        {
            $this->_db = $db;
        }

        /**
         * Récupère les informations sur les commandes en fonction du numéro
         *
         * @param integer $num_command Attend le numéro de commande en argument
         * @return object Retourne un objet commande
         */
        public function getNumCommand($num_command)
        {
            $num_command = (int) $num_command;

            $q = $this->_db->query('SELECT * FROM commands
            WHERE num_command = '.$num_command);

            $donnees = $q->fetch(PDO::FETCH_ASSOC);

            return new Commands($donnees);
        }

        /**
         * Récupère les commandes faites par un utilisateur en fonction de son id
         *
         * @param integer $user_id Attend l'id d'un utilisateur en argument
         * @return Array Retourne un tableau s'il existe des commandes. Sinon sort de la fonction.
         */
        public function get($user_id)
        {
            $user_id = (int) $user_id;

            $q = $this->_db->prepare('SELECT * FROM commands
            WHERE user_id = '.$user_id);

            $q->execute();

            foreach ($q as $key => $value) 
            {
                $command[] = array(
                    'num_command' => (int) $value['num_command'],
                    'date_command' => $value['date_command'],
                    'total_price' => (float) $value['total_price'],
                    'mode' => $value['mode']
                );
            }

            if(empty($command))
            {
                return;
            }
            else
            {
                return $command;
            }

            
        }

        /**
         * Récupère les informations de la commande en fonction de l'id de l'utilisateur, de la valeur de random et du prix total
         *
         * @param integer $user_id Attend l'id de l'utilisateur en argument
         * @param integer $random Attend la valeur de random en argument
         * @param float $total_price Attend le prix total de la commande en argument
         * @return object Retourne un objet Commands
         */
        public function getByUserId($user_id, $random, $total_price)
        {
            $user_id = (int) $user_id;
            $random = (int) $random;

            $q = $this->_db->query("SELECT * FROM commands
            WHERE user_id = '$user_id' AND random = '$random' AND total_price = '$total_price'");

            $donnees = $q->fetch(PDO::FETCH_ASSOC);

            return new Commands($donnees);
        }

        /**
         * Récupère les informations de la commande en fonction de l'id de l'utilisateur et de la valeur de random
         *
         * @param integer $user_id Attend l'id de l'utilisateur en argument
         * @param integer $random Attend la valeur de random en argument
         * @return object Retourne un objet Commands
         */
        public function getByRandom($user_id, $random)
        {
            $user_id = (int) $user_id;
            $random = (int) $random;

            $q = $this->_db->query("SELECT * FROM commands
            WHERE user_id = '$user_id' AND random = '$random'");

            $donnees = $q->fetch(PDO::FETCH_ASSOC);

            return new Commands($donnees);
        }

        /**
         * Ajoute une commande dans la base de données
         *
         * @param Commands $command Attend un objet commande en argument
         * @return void
         */
        public function add(Commands $command)
        {

            $q = $this->_db->prepare('INSERT INTO commands(user_id, date_command, total_price, random, mode)
            VALUES(:user_id, :date_command, :total_price, :random, :mode)');

            $q->bindValue(':user_id', $command->user_id());
            $q->bindValue(':date_command', $command->date_command());
            $q->bindValue(':total_price', $command->total_price());
            $q->bindValue(':random', $command->random());
            $q->bindValue(':mode', $command->mode());

            $q->execute();

        }

        /**
         * Récupère la commande d'un utilisateur en fonction du tableau que renvoi $_COOKIE['cartArticles']
         *
         * @param Array $myCommands Attend un tableau en argument
         * @param integer $user_id Attend l'id de l'utilisateur en argument
         * @return Array Retourne un tableau avec toutes les commandes de l'utilisateur
         */
        public function myCommands(Array $myCommands, $user_id)
        {

            foreach ($myCommands as $key => $value) 
            {
                $allCommands[$key] = array(
                        'userId' => (int) $user_id, 
                        'bookId' => (int) $value->id,
                        'name' => $value->name,
                        'price' => (int) $value->price,
                        'weight' => (int) $value->weight,
                        'qt' => (int) $value->qt,
                        'url' => $value->url
                    );
            }

            return $allCommands;

        }

        /**
         * Récupère les informations de la commande payée
         *
         * @param Array $myPaiedCommands Attend le tableau de la commande qui a été payé
         * @param integer $user_id Attend l'id de l'utilisateur en argument
         * @param string $mode Attend le mode de paiement en argument
         * @return Array Retourne un tableau des commandes payées
         */
        public function myPaiedCommands(Array $myPaiedCommands, $user_id, $mode)
        {

            foreach ($myPaiedCommands as $key => $value) 
            {
                $allPaiedCommands[$key] = array(
                        'userId' => (int) $user_id, 
                        'bookId' => (int) $value->id,
                        'name' => $value->name,
                        'price' => (int) $value->price,
                        'weight' => (int) $value->weight,
                        'qt' => (int) $value->qt,
                        'url' => $value->url,
                        'random' => random_int(24343, 2753778677852),
                        'mode' => $mode
                    );
            }

            return $allPaiedCommands;
        }

        /**
         * Envoie un mail de confirmation après le paiement d'une commande
         *
         * @param Users $userWhoPaied Atttend un objet de l'utilisateur qui a payé la commande en argument
         * @param object $mail Attend l'objet PHPMailer en argument
         * @param integer $num_command Attend le numéro de commande en argument
         * @param html $mailBody Attend le cors du mail en argument
         * @return void
         */
        public function sendConfirmationMail(Users $userWhoPaied, $mail, $num_command, $mailBody)
        {
    
            $mail->isSMTP();
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
            $mail->Host = 'smtp1.example.com;smtp2.example.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'user@example.com';
            $mail->Password = 'secret';
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;

            $mail->setFrom('from@example.com', 'Mailer');
            $mail->addAddress('joe@example.net', 'Joe User');
    
            $mail->isHTML(true);
            $mail->Subject = 'Objet du mail';
            $mail->Body = 'Corps du mail';
    
            $mail->send();
        }
    }

?>