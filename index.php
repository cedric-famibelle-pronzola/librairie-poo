<?php 

    session_start();

        /**
     * permet de lier l'instanciation de la casse avec les fichiers présents dans le dossier 'classes'
     * ----------------
     * s'exécute avec "spl_autoload_register('chargerClasse')"
     *
     * @param object $classe
     * @return void
     */
    function chargerClasse($classe)
    {
        require './classes/' . $classe . '.php';
    }

    include './connect/connect.php';            # les informations de connexion à la base de données
    spl_autoload_register('chargerClasse');     # permet de charger les classes

    $db = new PDO ("mysql:host=$server;dbname=$dbname;charset=utf8", $user, $pass_db);

    $usersManager = new UsersManager($db);
    $commandContentManager = new CommandContentManager($db);
    $booksManager = new BooksManager($db);
    $authorsManager = new AuthorsManager($db);

    if(isset($_COOKIE['register']))
    {
        $reg = true;
        $regSentence = '<p>Votre inscription a été validé, veuillez vous connecter</p>';
        setcookie('register', null, -1, '/');
    }
    else
    {
        $reg = false;
    }

    if(isset($_COOKIE['wrong_email']))
    {
        $wrong_mail = true;
        $wrong_sentence = '<p>Votre adresse mail " ' . $_COOKIE['wrong_email'] . ' " est inconnue.</p>';
        setcookie('wrong_email', null, -1, '/');
    }
    else
    {
        $wrong_mail = false;
    }

    if(isset($_COOKIE['wrong_password']))
    {
        $wrong_password = true;
        $wrong_password_sentence = '<p>Le mot de passe est incorrect</p>';
        setcookie('wrong_password', null, -1, '/');
    }
    else
    {
        $wrong_password = false;
    }
    
    if(isset($_COOKIE['connectToBook']))
    {

        $connected = true;
        $user_random = $_COOKIE['connectToBook'];
        $user = $usersManager->getByRandom($user_random);
        $_SESSION['user_id'] = $user->user_id();
        $_SESSION['first_name'] = $user->first_name();
        $_SESSION['last_name'] = $user->last_name();
        $_SESSION['email'] = $user->email();
        $_SESSION['phone'] = $user->phone();
        $_SESSION['address'] = $user->address();
        $_SESSION['zip_code'] = $user->zip_code();
        $_SESSION['city'] = $user->city();
        setcookie('booksifa', null, -1, "/");

    }
    else
    {
        $connected = false;
        setcookie('booksifa', 'visiteur', time() + 365*24*3600, "/");
        $_SESSION['first_name'] = 'Visiteur'; 
    } 

    
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Accueil | Books'IFA by Cédric FAMIBELLE-PRONZOLA</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link rel="icon" href="./ifa/icon.ico" />
        <link href="./script/css/bootstrap.min.css" rel="stylesheet">
        <link href="./script/css/mdb.min.css" rel="stylesheet">
        <link href="./script/css/style.css" rel="stylesheet">
    </head>
    <body>

        <?php

            include 'menu.php';
            if(!$connected)
            {
                include 'log_sign.html';
            }

        if($reg)
        echo $regSentence;

        if($wrong_mail)
        echo $wrong_sentence;

        if($wrong_password)
        echo $wrong_password_sentence;

        ?>
            <div class="container-fluid">
        <?php
        if($connected)
        {
            echo 'Bienvenue ' . $user->first_name();
        }
        elseif(!isset($_COOKIE['register']) && $connected == false)
        {
            echo '<p>Veuillez vous inscrire ou vous connecter.<p>';
        }
    
        $requestChoice = $booksManager->bestSeller();

        if(empty($requestChoice))
        {
        ?>

        <h2 class="text-center">Aucune vente pour le moment</h2>  
        <?php 
        }
        else
        {
        ?>
            <h2 class="text-center">Nos 3 meilleures ventes</h2>
        <?php
            
        }

        ?>

        <div class="d-flex justify-content-center row mt-3">

    <?php

        for($i = 0; $i < count($requestChoice); $i++)
        {
    ?>
            <div class="card promoting-card col-2 col-sm-3 col-md-2 ml-3 mb-5">
                <div class="card-body d-flex flex-row">
                    <div>
                        <a data-toggle="tooltip" data-placement="bottom" title="<?php echo $requestChoice[$i]->title() ?>" href="./pages/fiches.php?book_id=<?php echo $requestChoice[$i]->book_id() ?>"> <h5 style="max-width: 21vh;" class="card-title font-weight-bold mb-2 text-danger text-truncate"><?php echo $requestChoice[$i]->title() ?> </h5></a>
                        <p class="card-text"><i class="fas fa-portrait pr-2"></i> <a href="./pages/fiches_authors.php?author_id=<?php echo $authorsManager->getByBookId($requestChoice[$i]->book_id())->author_id()?>">
                        <?php echo $authorsManager->getByBookId($requestChoice[$i]->book_id())->name() ?></a></p>
                        <p class="card-text text-success"><?php echo $requestChoice[$i]->price() ?> <i class="fas fa-euro-sign pr-2"></i></p>
                    </div>
                </div>
                <div class="view overlay">
                    <img style="max-width: 250px; max-height: 300px;" class="card-img-top rounded-0" src="./img/<?php echo $requestChoice[$i]->img() ?>" alt="Card image cap">
                    <a href="./pages/fiches.php?book_id=<?php echo $requestChoice[$i]->book_id() ?>">
                    <div class="mask rgba-white-slight"></div>
                    </a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <button type="button" data-id="<?php echo $requestChoice[$i]->book_id() ?>" data-name="<?php echo $requestChoice[$i]->title() ?>" data-price="<?php echo $requestChoice[$i]->price() ?>" data-weight="10" data-url="./fiches.php?book_id=<?php echo $requestChoice[$i]->book_id() ?>" class="add-to-cart fas fa-cart-plus fa-2x text-muted p-1 my-1 mr-3 rounded" title="Article ajouté"></button>
                        <div class="def-number-input number-input">
                            <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus"></button>
                                <input class="qt quantity" min="1" name="quantity" value="1" type="number">
                            <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus"></button>
                        </div>
                    </div>
                </div>
            </div>
        <?php    
            }
        ?>
        </div>
        <script src="./script/js/jquery-3.3.1.min.js"></script>
        <script src="./script/js/popper.min.js"></script>
        <script src="./script/js/bootstrap.min.js"></script>
        <script src="./script/js/mdb.min.js"></script>
        <script src="./script/js/main.js"></script>
    </body>

</html>