<nav class="navbar navbar-expand-lg sticky-top navbar-dark bg-dark">
    <a class="navbar-brand" href="./index.php">
        <img src="./ifa/icon.ico" height="40" alt="books'IFA logo">
    </a>
    <div class="flex1">
       <a href="./index.php"><h3 class="amber-text font-weight-bold">Bienvenue sur Books'IFA</h3></a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
    aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse flex2" id="navbarSupportedContent-555">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="./pages/authors.php">
                <i class="fas fa-portrait"></i> Auteurs
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="./pages/books.php">
                <i class="fas fa-book"></i> Livres
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="./contact.php">
                <i class="far fa-envelope"></i> Contact
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="./pages/cart.php">
                <span class="in-cart-items-num">0</span>
                <i class="fas fa-shopping-cart"></i> Panier
                </a>
            </li>
            <?php
                if(isset($_COOKIE['connectToBook']))
                {
            ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <i class="fas fa-user"></i><?php if(isset($_SESSION['first_name'])) echo $_SESSION['first_name']?></a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
                        <?php if($_SESSION['first_name'] == 'admin')
                                {
                        ?>
                                    <a class="dropdown-item" href="./admin.php">Administration</a>
                        <?php
                                } 
                        ?>
                        <a <?php if($_SESSION['first_name'] == 'admin') echo 'style="display:none;">' ?> class="dropdown-item" href="./profil.php">Mon compte</a>
                        <a class="dropdown-item" href="./log_out.php">Se déconnecter</a>
                    </div>
                </li>
            <?php
                }
            ?>
        </ul>
    </div>
</nav>