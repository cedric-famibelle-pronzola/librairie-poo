<?php
    session_start();
    include '../connect/connect.php';               # les informations de connexion à la base de données
    include '../connect/functions.php';             # regroupe les fonctions 
    spl_autoload_register('chargerClasse');         # permet de charger les classes

    $db = new PDO ("mysql:host=$server;dbname=$dbname;charset=utf8", $user, $pass_db);

    $booksManager = new BooksManager($db);
    $authorsManager = new AuthorsManager($db);
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Fiches : <?php if(isset($_GET['book_id'])) $book_id = $_GET['book_id']; echo $booksManager->get($book_id)->title() ?> | Books'IFA by Cédric FAMIBELLE-PRONZOLA</title>
        <link rel="icon" href="../ifa/icon.ico" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="../script/css/bootstrap.min.css" rel="stylesheet">
        <link href="../script/css/mdb.min.css" rel="stylesheet">
        <link href="../script/css/style.css" rel="stylesheet">
    </head>
    <body>
        <?php
        
        include './menu.php';
            
        if(!isset($_COOKIE['connectToBook']))
        {
            include 'log_sign.html';
        }

        ?>
        
        <?php
        

            if(isset($_GET['book_id']))
            {
                $book_id = $_GET['book_id'];
            ?>
        <div class="d-flex flex-row justify-content-around">
            <div class="container pt-3 offset-md-2 col-6">
                <div class="jumbotron text-primary text-center hoverable p-4">

                    <div class="row">
                            <div class="col-md-4 mx-3 my-3">
                                    <img style="width:80%;" src="<?php echo '../img/'.$booksManager->get($book_id)->img() ?>" class="img-fluid" alt="<?php echo $booksManager->get($book_id)->title() ?>">

                            </div>
                        
                        <div class="col-md-7 text-md-left mt-3">
                            <p class="title h1"> Titre : <?php echo $booksManager->get($book_id)->title() ?> </p>
                            <p class="h3"> Auteur : <a class="text-danger" href="./fiches_authors?author_id=<?php echo $authorsManager->getByBookId($book_id)->author_id() ?>"> <?php echo $authorsManager->getByBookId($book_id)->name() ?></a></p>
                            <p class="h3"> Date de sortie : <?php echo $booksManager->get($book_id)->release_date() ?></p>
                            <p class="h3"> Edition : <?php echo $booksManager->get($book_id)->edition()  ?></p>
                            <p class="h3"> Catégorie : <?php echo $booksManager->get($book_id)->category() ?></p>
                            <p class="h3"> Format : <?php echo $booksManager->get($book_id)->format() ?></p>
                            <p class="h3"> Prix : <?php echo $booksManager->get($book_id)->price() . ' €' ?></p>
                        </div>
                    </div>  
                </div>
            </div>
            <?php                   
            }

        ?>
            <div class="container h5">
                <p>
                <div class="row">
                    <button title="Article ajouté" type="button" class="add-to-cart fas fa-cart-plus fa-2x p-1 my-1 mr-3 pr-3 rounded" data-id="<?php echo $booksManager->get($book_id)->book_id() ?>" data-name="<?php echo $booksManager->get($book_id)->title() ?>" data-price="<?php echo $booksManager->get($book_id)->price() ?>" data-weight="10" data-url="./fiches.php?book_id=<?php echo $booksManager->get($book_id)->book_id() ?>"></button>
                    <div class="def-number-input bg-white number-input">
                        <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus"></button>
                            <input class="qt quantity" min="1" name="quantity" value="1" type="number">
                        <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus"></button>
                    </div>
                </div>

                </p>
                <div class="dropdown">
                    <div id="cart">
                        <p><span class="in-cart-items-num">0</span> Articles</p>
                    </div>
                    <ul id="cart-dropdown" style="display : none;">
                        <li id="empty-cart-msg"><a>Votre panier est vide</a></li>
                        <li class="go-to-cart hidden"><a href="../pages/cart.php">Voir le panier</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <script src="../script/js/jquery-3.3.1.min.js"></script>
        <script src="../script/js/popper.min.js"></script>
        <script src="../script/js/bootstrap.min.js"></script>
        <script src="../script/js/mdb.min.js"></script>
        <script src="../script/js/addons/datatables.js"></script>
        <script src="../script/js/main.js"></script>
    </body>

</html>