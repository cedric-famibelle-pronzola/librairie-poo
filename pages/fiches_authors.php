<?php
    session_start();
    include '../connect/connect.php';               # les informations de connexion à la base de données
    include '../connect/functions.php';             # regroupe les fonctions 
    spl_autoload_register('chargerClasse');         # permet de charger les classes

    $db = new PDO ("mysql:host=$server;dbname=$dbname;charset=utf8", $user, $pass_db);

    $booksManager = new BooksManager($db);
    $authorsManager = new AuthorsManager($db);
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Fiches : <?php if(isset($_GET['author_id'])) $author_id = $_GET['author_id']; echo $authorsManager->getAuthorId($author_id)->name() ?> | Books'IFA by Cédric FAMIBELLE-PRONZOLA</title>
        <link rel="icon" href="../ifa/icon.ico" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="../script/css/bootstrap.min.css" rel="stylesheet">
        <link href="../script/css/mdb.min.css" rel="stylesheet">
        <link href="../script/css/style.css" rel="stylesheet">
    </head>
    <body class="container-fluid pt-3">

        <?php

            if(isset($_GET['author_id']))
            {
                $book_id = $_GET['author_id'];
            ?>
                    <p> Nom : <?php echo $authorsManager->getAuthorId($author_id)->name() ?></p>
                    <p> Sexe : <?php echo $authorsManager->getAuthorId($author_id)->sexe() ?></p>
                    <p> Date de Naissance : <?php echo $authorsManager->getAuthorId($author_id)->birth_date() ?></p>
                    <p> Biographie : <?php echo $authorsManager->getAuthorId($author_id)->biography() ?></p>
                    <p> Livres : <?php echo $authorsManager->getAuthorId($author_id)->name() ?></p>
            <?php
            }
        ?>
        
        <script src="../script/js/jquery-3.3.1.min.js"></script>
        <script src="../script/js/popper.min.js"></script>
        <script src="../script/js/bootstrap.min.js"></script>
        <script src="../script/js/mdb.min.js"></script>
        <script src="../script/js/addons/datatables.js"></script>
        <script src="../script/js/main.js"></script>
    </body>

</html>